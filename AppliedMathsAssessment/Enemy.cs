﻿using System;
using Microsoft.Xna.Framework;

namespace AppliedMathsAssessment
{
    class Enemy : ModelObject
    {
        // ------------------
        // Data
        // ------------------
        private Player target;
        private const float TURN_SPEED = 2f;
        private const float MOVE_ACCEL = 75f;

        // ------------------
        // Behaviour
        // ------------------
        public void Initialise(Vector3 newPosition, Player newTarget)
        {
            target = newTarget;
            position = newPosition;
            isTrigger = false;
        }
        // ------------------
        public void SetTarget(Player newTarget)
        {
            target = newTarget;
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Set our acceleration this frame to 0
            acceleration = new Vector3(0, 0, 0);

            // Determine direction to target
            Vector3 targetPos = target.GetPosition();
            Vector3 direction = (targetPos - position);
            direction.Normalize(); // Set direction vector to length 1
            float targetAngle = (float)Math.Atan2(direction.X, direction.Z);
            float currentAngle = rotation.Y;
            float angleDiff = targetAngle - currentAngle;

            // Wrap around, get angle in terms of left or right (between -Pi and Pi)
            while (angleDiff > Math.PI)
                angleDiff -= 2f * (float)Math.PI;
            while (angleDiff < -Math.PI)
                angleDiff += 2f * (float)Math.PI;

            // Rotate towards player
            float distanceToTurn = TURN_SPEED * dt;
            if (Math.Abs(angleDiff) < distanceToTurn) // Would pass target angle, so just set to it
                rotation.Y = targetAngle;
            else if (angleDiff < 0f) // Should turn left
                rotation.Y -= distanceToTurn;
            else if (angleDiff > 0f) // Should turn right
                rotation.Y += distanceToTurn;

            // Accelerate towards the player
            acceleration.X = (float)Math.Sin(rotation.Y) * MOVE_ACCEL; // Don't need to use dt as we're setting, not adding
            acceleration.Z = (float)Math.Cos(rotation.Y) * MOVE_ACCEL; // Don't need to use dt as we're setting, not adding

            base.Update(gameTime);
        }
        // ------------------

    }
}
