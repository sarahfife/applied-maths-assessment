﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

namespace AppliedMathsAssessment
{
    class PopUp : Sprite
    {
        // ------------------
        // Data
        // ------------------
        Text popUpText;
        Button popUpButton;
        int buttonWidth;

        bool showing = false;
        float showTimePassed = 0;
        const float ANIMATION_TIME = 1f;
        Vector2 OffscreenPos;
        Vector2 OnscreenPos;


        // ------------------
        // Behaviour
        // ------------------
        public PopUp(
            Texture2D newTexture, 
            Texture2D newButtonTexture, 
            Texture2D newPressedTexture, 
            SpriteFont newButtonFont, 
            SoundEffect newClickSFX, 
            SpriteFont newPanelFont,
            string panelText,
            string buttonText,
            GraphicsDevice graphics)
            : base(newTexture)
        {
            popUpText = new Text(newPanelFont);
            popUpText.SetTextString(panelText);
            popUpText.SetAlignment(Text.TextAlignment.CENTRE);
            popUpText.SetColor(Color.Black);
            popUpButton = new Button(newButtonTexture, newPressedTexture, newButtonFont, newClickSFX);
            popUpButton.SetString(buttonText);
            buttonWidth = newButtonTexture.Width;

            OnscreenPos = new Vector2(
                graphics.Viewport.Bounds.Width / 2 - newTexture.Width / 2, 
                graphics.Viewport.Bounds.Height / 2 - newTexture.Height / 2);
            OffscreenPos = OnscreenPos;
            OffscreenPos.Y = 1000;

            SetPosition(OffscreenPos);
        }
        // ------------------
        public override void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
            popUpText.SetPosition(position + new Vector2(texture.Width / 2, +50f));
            popUpButton.SetPosition(position + new Vector2(texture.Width / 2 - buttonWidth / 2, +100f));
        }
        // ------------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            popUpText.Draw(spriteBatch);
            popUpButton.Draw(spriteBatch);
        }
        // ------------------
        public bool IsClicked()
        {
            return popUpButton.IsClicked();
        }
        // ------------------
        public void Show()
        {
            if (!showing)
            {
                showing = true;
                showTimePassed = 0;
            }
        }
        // ------------------
        public void Hide()
        {
            if (showing)
            {
                showing = false;
                SetPosition(OffscreenPos);
            }
        }
        // ------------------
        public void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (showing)
            {
                // update timer
                showTimePassed += dt;

                ///////////////////////////////////////////////////////////////////
                //
                // CODE FOR TASK 4 SHOULD BE ENTERED HERE
                //
                ///////////////////////////////////////////////////////////////////

                // OLD VERSION, DEPRECATED
                // Re-write this to move the pop-up using a cubic interpolation
                SetPosition(OnscreenPos);

                ///////////////////////////////////////////////////////////////////  
                // END TASK 4 CODE
                ///////////////////////////////////////////////////////////////////  
            }

        }
        // ------------------
    }
}
