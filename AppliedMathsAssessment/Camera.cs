﻿using Microsoft.Xna.Framework;

namespace AppliedMathsAssessment
{
    public struct Camera
    {
        // ------------------
        // Data
        // ------------------
        public PhysicsObject targetObject;  // What the camera is looking at
        public Vector3 offset;              // The camera's position relative to the target
        public Vector3 whichWayIsUp;        // The up direction
        public float fieldOfView;           // The wideness of the camera lense angle
        public float aspectRatio;           // The screen's aspect ratio
        public float nearPlane;             // Things closer than this won't be drawn
        public float farPlane;              // Things farther than this won't be drawn

        public Vector3 position
        {
            get { return targetObject.GetPosition() + offset; }
        }
        public Vector3 target
        {
            get { return targetObject.GetPosition(); }
        }
    }
}
